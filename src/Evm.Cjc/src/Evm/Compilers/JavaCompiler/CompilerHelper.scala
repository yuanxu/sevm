
package evm.compilers.javaCompiler

/**
 * 编译器辅助类
 */
import evm.CompileError
import evm.DataType
import evm.schema.Entity
import evm.schema.Method
import evm.schema.SchemaException
import evm.schema.Statement
import evm.schema.Property
import evm.schema.TransactionModel
import scala.collection.mutable.StringBuilder
import evm.utils.{DbDriverUtil, NameUtil, RefUtil}

object CompilerHelper {
  private val CR="\r\n"

  /**
   * 获取语句签名
   */
  def getSign( stm : Statement):String = getSign(stm.resultClass,stm.id,stm.parametersDefinition,stm.isReturnList,stm.supportPaging,stm.supportSort)

  /**
   * 获取方法签名
   */
  def getSign(method:Method):String = getSign(method.resultClass,method.id,method.parametersDefinition,method.isReturnList,method.supportPaging,method.supportSort)

  /**
   * 获取签名
   */
  private def getSign(resultClass:String,id:String,parameters:List[Pair[String, String]],isReturnList:Boolean,supportPaging:Boolean,supportSort:Boolean)={
    var result = ""
    if(isReturnList)
    {
      if(supportPaging)
        result = "PagedList<" +DataType.getJvmDataType(resultClass)+">"
      else
        result = "List<"+DataType.getJvmDataType(resultClass)+">"
    }
    else
    {
      if(resultClass.isEmpty)
        result = "void"
      else
        result =DataType.getJvmDataType(resultClass)
    }
    var ps=""
/*
    if(supportSort){
      if(!parameters.isEmpty)
        ps =","
      ps  += "String sortColumn,String sortDirection"
    }
    */

    //修正参数
    val buffer = new StringBuilder()

    parameters.map(p => Pair(DataType.getJvmDataType(p._1),p._2)).foreach(p=>{
        if(!buffer.isEmpty)
          buffer.append(",")
        buffer.append(("%s %s").format(p._1,p._2))}
    )
    ("%s %s ( %s%s )").format(result,id,buffer.toString(),ps)
  }

  /**
   * Dao实现
   */
  def getDaoImpl(stm:Statement)={
    val isUpdate = stm.tagName=="update"
    var qp = "null" //传递给myMapp的参数。默认空
    val builder =new StringBuilder()
    //如果有参数，则准备参数
    if(stm.hasParameter){
      if(stm.supportPaging && stm.parametersDefinition.length == 3 ){ // evm会自动为支持分页的语句增加两个参数，所以如果有三个参数，则实际上有两个参数是需要直接传递的
        qp = stm.parametersDefinition(0)._2 //参数名
      }
      else if(isUpdate || stm.parametersDefinition.length>1) //多个参数
      {
        qp="ht"//多个参数则会封装到HashMap中，变量名为ht
        builder.append("HashMap ht = new HashMap();"+CR)
        for{p <- stm.parametersDefinition;
            if(!(stm.supportPaging &&(p._2 == "pageIndex"|| p._2 == "pageSize"))); //排除支持分页时添加的pageIndex和pageSize参数
            if(!(stm.supportSort && (p._2=="sortColumn" || p._2 == "sortColumn"))) //排除支持排序时添加的参数
              }
            {
            builder.append(("ht.put(\"%s\",%s);"+CR).format(p._2,p._2))
          }
            if(stm.supportSort){
            builder.append("getSortHashtable(ht,sortColumn,sortDirection);"+CR)
          }
            if(isUpdate){
            builder.append(("ht.put(\"%s\",new java.sql.Timestamp(java.util.Calendar.getInstance().getTimeInMillis()));"+CR).format(NameUtil.getJvmPropertyName("EvmLastModifyTime")))//TODO:DateTime.Now
          }
        }
        else
        {
          qp = stm.parametersDefinition(0)._2
        }
      } //准备完了参数

      //准备调用语句
      if(stm.supportPaging && stm.supportSort){
        builder append ("return new PagedList<%1$s>(pageIndex ,pageSize, getMapper().queryForList(\"%2$s.%3$s\",%4$s,%5$s,%6$s) , getInt(getMapper().queryForObject(\"%2$s.%3$sCount\",%4$s),-1));"+CR).format(
                              stm.resultClass
                              ,stm.entity.name
                              ,stm.id
                              ,qp
                              ,"(pageIndex-1)*pageSize"
                              , "pageSize")
      }else if(stm.isReturnList){
        builder append ("return (List<%1$s>) getMapper().queryForList(\"%2$s.%3$s\",%4$s);"+CR).format(
                              stm.resultClass
                              ,stm.entity.name
                              ,stm.id
                              ,qp)
      }else if(stm.tagName=="select"){
        if(stm.resultClass == "int"){
        builder append ("return getInt(getMapper().queryForObject(\"%1$s.%2$s\",%3$s),-1);"+CR).format(
                              stm.entity.name
                              ,stm.id
                              ,qp)
        }else{
        builder append ("return (%1$s)getMapper().queryForObject(\"%2$s.%3$s\",%4$s);"+CR).format(
                              stm.resultClass
                              ,stm.entity.name
                              ,stm.id
                              ,qp)
        }
      }else{
        var ret = ""
        if(!stm.resultClass.isEmpty)
          builder append "return "
        if(stm.resultClass=="int" && stm.tagName!="update"){
          builder append " getInt("
          builder append ("getMapper().%1$s(\"%2$s.%3$s\",%4$s),-1);"+CR).format(
                              stm.tagName
                              ,stm.entity.name
                              ,stm.id
                              ,qp)
        }
        else{
        builder append ("(%1$s)getMapper().%2$s(\"%3$s.%4$s\",%5$s);"+CR).format(
                              stm.resultClass
                              ,stm.tagName
                              ,stm.entity.name
                              ,stm.id
                              ,qp)
        }

      }

      builder.toString()
    }

    /**
     * 获取服务实现
     */
    def getSvcImpl(method:Method)={
      var result=""
      if(method.isReturnList )
        if(method.supportPaging)
          result = ("PagedList<%s>").format(method.resultClass)
      else
        result = ("List<%s").format(method.resultClass)
      else if(method.resultClass.isEmpty)
        result="void"
      else
        result = method.resultClass

      if(result!="void" && method.transactionModel == TransactionModel.RequiredNew)
        throw new SchemaException((CompileError.ER0035.toString).format(method.service.name,method.id)) // 服务定义错误[%s]：%s 声明需要新事务(RequiredNew)的方法，Evm要求不能具有返回值

      val builder = new StringBuilder()
      if(!method.refStatement.isEmpty)
      {
        prepareTrans(method, builder)

        if(result != "void")
          builder append "return "

        val referedEntity = RefUtil.getSimpleRefPart(method.refStatement)._1 // method.beReferencedStatements(0).entity.name
        val call = RefUtil.replace(method.refStatement, referedEntity, "get"+referedEntity + "Dao()")
        builder.append(call)
        builder.append("(")
        var added = false;
        if (method.hasParameter) //有参数
        {
          for ( kv <- method.parametersDefinition)          {
            if (added)
              builder.append(",")
            builder.append(kv._2)
            added = true
          }
        }
        /*if(method.supportSort){
            if(added)
              builder append ","
            builder append("sortColumn, sortDirection")
          }*/
        builder.append(");")
        finishTrans(method, builder)
      }
      else //原生代码
      {
        //准备事务语句
        prepareTrans(method, builder)
        var code = method.embedCode

        for ( item <- method.referencedMethods)
        {
          var tsvc = ""
          if (item.service.name == method.service.name)
            tsvc = "this"
          else
            tsvc = "get"+item.service.name+"()"
          code = RefUtil.replace(code, item.service.name, tsvc)
        }
        for (item <- method.referencedStatements)
        {
          code = RefUtil.replace(code, item.entity.name, "get"+item.entity.name + "Dao()")
        }
        builder.append(code)
        finishTrans(method, builder)
      }

      builder.toString()
    }

    /**
     * 获取SqlMap语句实现
     */
    def getStmImpl(stm:Statement) = {
      var sql = stm.innerXml

      if(stm.tagName=="update"){
        val iloc = sql.toLowerCase.indexOf("where")
        sql = ("%s,EvmLastModifyTime=#evmLastModifyTime# %s").format(sql.substring(0,iloc),sql.substring(iloc))
      }

      var sortSql = sql;
      if (stm.supportSort)
      {
        sortSql = sql + " <dynamic prepend='ORDER BY'><isPropertyAvailable property='sortColumn'>$sortColumn$ $sortDirection$</isPropertyAvailable></dynamic>"
      }
      var stmParameters=""
      if(stm.parametersDefinition.length == 1)
        stmParameters = ("parameterClass='%s'").format(stm.parametersDefinition(0)._2)
      else if(stm.parametersDefinition.length > 1)
        stmParameters = "parameterClass='HashMap'"

      var result = ""
      if(!stm.resultClass.isEmpty)
        result = ("resultClass='%s'").format(stm.resultClass)

      var cacheModel=""
      if(!stm.cacheModel.isEmpty)
        cacheModel=("cacheModel='%s'").format(stm.cacheModel)

      ("<%s id='%s' %s %s %s> %s </%1$s>").format(
             stm.tagName
             ,stm.id
             ,stmParameters
             ,result
             ,cacheModel
             ,sortSql)
    }

    /**
     * 获取Jvm风格名称
     */
    private def  getName(name:String, clrStyle:Boolean = false) ={
      if(!clrStyle)
        NameUtil.getJvmMethodName(name)
      else
        NameUtil.getClrName(name)
    }

    /**
     * 获取Id名称
     */
    private def getId(clrStyle:Boolean)= if(clrStyle) NameUtil.clrIDName else NameUtil.jvmIdName

    //<editor-fold defaultstate="collapsed" desc="获取默认的语句声明">
    /**
     * 获取默认的语句声明
     */
    def getDefaultStmImpl(entity:Entity)={
      val builder = new StringBuilder()
      val clrStyle=entity.app.options.useClrStyle
      val id = getId(clrStyle)
      val uid = if(clrStyle) "UID" else "UId"

      builder append
      <insert id={getName("Add",clrStyle)} paramenterClass={entity.name}>
		INSERT INTO {entity.dbName} ( {getFileds(entity,p => p.column)} ) VALUES ( {getFileds(entity,p => if(p.dataType == "byte[]") ("#%s:%s#").format(p.name,p.dbType) else ("#%s#").format(p.name) )} )
        {getSelectKey(entity)}
      </insert>

      builder append CR

      if (entity.app.options.generateIntegerId)
      {
        builder append
        <update id={getName("Save",clrStyle)} parameterClass={entity.name}>
          UPDATE {entity.dbName} SET {getFileds(entity,p =>if(p.dataType == "byte[]") ("%1$s=#%1$s:%2$s#").format(p.name,p.dbType) else ("%1$s=#%1$s#").format(p.name))} WHERE {entity.name}ID = #{entity.name}ID#
        </update>
        builder append CR

        builder append
        <delete id={getName("DeleteBy"+id,clrStyle)} paramenterClass={DataType.getJvmDataType(entity.app.options.integerIdDataType)}>
          DELETE FROM {entity.dbName} WHERE {entity.name+id}=#value#
        </delete>
        builder append CR

        builder append
        <select id={getName("GetBy"+id,clrStyle)} parameterClass={DataType.getJvmDataType(entity.app.options.integerIdDataType)} resultClass={entity.name}>
          SELECT * FROM {entity.dbName} WHERE {entity.name+id}=#value#
        </select>
      }
      else
      {
        builder append
        <update id={getName("Save",clrStyle)} parameterClass={entity.name}>
          UPDATE {entity.dbName} SET {getFileds(entity,p =>if(p.dataType == "byte[]") ("%1$s=#%1$s:%2$s#").format(p.name,p.dbType) else ("%1$s=#%1$s#").format(p.name))} WHERE {entity.name+uid} = #{entity.name+uid}#
        </update>
      }

      builder append CR

      builder append
      <delete id={getName("DeleteBy"+uid,clrStyle)} paramenterClass={DataType.getJvmDataType("guid")}>
		DELETE FROM {entity.dbName} WHERE {entity.name+uid}=#value#
      </delete>

      builder append CR

      builder append
      <select id={getName("GetBy"+uid,clrStyle)} parameterClass={DataType.getJvmDataType("guid")} resultClass={entity.name}>
		SELECT * FROM {entity.dbName} WHERE {entity.name+uid}=#value#
      </select>

      builder append CR

      builder append
      <select id={getName("GetList",clrStyle)} resultClass={entity.name} parameterClass="HashMap">
        SELECT * FROM {entity.dbName}
        <dynamic prepend="ORDER BY">
          <isPropertyAvailable  property="sortColumn">
            $sortColumn$ $sortDirection$
          </isPropertyAvailable>
        </dynamic>
      </select>

      builder append CR

      builder append
      <select id={getName("GetListCount",clrStyle)} resultClass="int" parameterClass="HashMap">
        SELECT COUNT(*) FROM {entity.dbName}
      </select>

      builder.toString()
    }
    //</editor-fold>



    /**
     * 获取字段
     */
    private def getFileds(entity:Entity, func : Function1[Property,String])=
    {
      val builder = new StringBuilder()
      val id=  if(entity.app.options.useClrStyle) NameUtil.clrIDName else NameUtil.jvmIdName

      entity.properties.foreach(p =>
        {
          if (p.name != entity.app.name + id && !p.noMap && p.select.isEmpty)
          {
            if(!builder.isEmpty)
              builder.append(",")
            builder.append(func(p))
          }
        });
      builder.toString()
    }

    private def  getSelectKey(entity:Entity)=DbDriverUtil.getSelectKey(entity)



    /// <summary>
    /// 准备事务
    /// </summary>
    /// <param name="method"></param>
    /// <param name="sbStm"></param>
    private def prepareTrans( method:Method,sbStm:StringBuilder)
    {
      if (method.transactionModel == TransactionModel.Required) //添加检验事务是否存在语句
        sbStm.append("assertInTransaction();"+CR)
      else if (method.transactionModel == TransactionModel.NotSupport) //添加检验事务不存在语句
        sbStm.append("assertNotInTransaction();"+CR);
      else if (method.transactionModel == TransactionModel.RequiredNew)//添加事务管理
      {
        sbStm.append(" assertTransactionNotStart();"+CR);
        sbStm.append("beginTransaction();"+CR);
        sbStm.append("try"+CR);
        sbStm.append("{"+CR);
      }
    }

    private def finishTrans( method:Method, sbStm:StringBuilder)
    {
      if (method.transactionModel == TransactionModel.RequiredNew)
      {
        sbStm.append("commit();"+CR);
        sbStm.append("}"+CR);
        sbStm.append("catch"+CR);
        sbStm.append("{"+CR);
        sbStm.append("rollback();"+CR);
        sbStm.append("throw;"+CR);
        sbStm.append("}"+CR);
      }

    }
  }