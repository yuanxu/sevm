
package evm.compilers.javaCompiler

import evm.EvmObjectCompilerBase
import evm.Log
import evm.utils.TplUtil
import java.io.File

private[javaCompiler] class IServiceCompiler (override val isEntityCompiler: Boolean = false,
                        override val isServiceCompiler: Boolean = true) extends EvmObjectCompilerBase{

  /**
   * 执行编译
   */
  override def doCompile(t: AnyRef): String = {
    try {
      t match {
        case svc: evm.schema.Service => 
          TplUtil.parse(getClass().getResource("templates/IService.ssp").getPath(), Map("svc" -> t))
        case _ => ""
      }
    } catch {
      case e @ _ =>
        Log.error(e.getMessage())
        throw e
    }
  }

  /**
   * 结束编译
   */
  override def endCompile(t: AnyRef, code: String) {
    super.endCompile(t, code)
    val pathName = format("%s/apps/%s/src/%s/service", tempDir,app.name, app.namespace.split('.').mkString("/"))
    val file = new File(pathName)
    
    if (!file.exists())
      file.mkdirs()
    TplUtil.writeToFile(format("%s/I%s.java", pathName, (t.asInstanceOf[evm.schema.Service]).name), code)
  }
}