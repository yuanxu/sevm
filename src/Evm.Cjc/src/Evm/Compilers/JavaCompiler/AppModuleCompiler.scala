package evm.compilers.javaCompiler

import evm.utils.TplUtil
import evm.{Log, EvmObjectCompilerBase}
import java.io.File

/**
 * Created by IntelliJ IDEA.
 * User: YuanXu
 * Date: 11-11-22
 * Time: 下午6:31
 * To change this template use File | Settings | File Templates.
 */

private[javaCompiler] class AppModuleCompiler  (override val isEntityCompiler: Boolean = true,
                      override val isServiceCompiler: Boolean = false) extends EvmObjectCompilerBase{

  /**
   * 执行编译
   */
  override def doCompile(t: AnyRef): String = {
    try {
      t match {
        case app: evm.schema.App =>
          TplUtil.parse(getClass().getResource("templates/AppModule.ssp").getPath(), Map("app" -> t))
        case _ => ""
      }
    } catch {
      case e @ _ =>
        Log.error(e.getMessage())
        throw e
    }
  }

  /**
   * 结束编译
   */
  override def endCompile(t: AnyRef, code: String) {
    super.endCompile(t, code)
    val pathName = format("%s/apps/%s/src/%s", tempDir,app.name, app.namespace.split('.').mkString("/"))
    val file = new File(pathName)

    if (!file.exists())
      file.mkdirs()
    TplUtil.writeToFile(format("%s/%sModule.java", pathName, (t.asInstanceOf[evm.schema.App]).name), code)
  }
}

