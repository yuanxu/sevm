/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package evm.Compilers.JavaCompiler.Test

import org.junit.Test
import evm.AppBuilder
import evm.compilers.javaCompiler.CoreJavaCompiler
import org.slf4j.LoggerFactory

class JavaCompilerTest {
  @Test
  def compile(){
    val log = LoggerFactory.getLogger(getClass())
    log.info("Start JavaCompilerTest")
    val compiler = new CoreJavaCompiler()
    val fileName = getClass().getResource("../../../../app.xml")
    println(fileName)
    val app = AppBuilder.parse(fileName.getPath)
    //println(fileName)
    val dir = getClass().getResource("../../../../");
    println(dir)
    compiler.setup(app, dir.getPath+"/out", dir.getPath+"/temp")
    compiler.compile
    
  }
}
