/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package evm.Compilers.JavaCompiler.Test
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Suite


@RunWith(classOf[Suite])
@Suite.SuiteClasses(Array( classOf[JavaCompilerTest]))
class CoreJavaCompilerTestSuit