package evm
import evm.schema.serializer.AppSerializer
import scala.collection.mutable.ListBuffer
import evm.schema.serializer.EntitySerializer
import evm.schema.serializer.ServiceSerializer
import evm.utils.RefUtil
import evm.utils.AppUtil
import evm.schema.SchemaException

/**
 * Evm App构建器
 */
object AppBuilder extends LogTrait {

  /**
   * 解析app定义为内存对象。同时根据app定义自动调整
   */
  def parse(fileName: String): evm.schema.App = {
    val app = AppSerializer.deserialize(fileName)
    //拆分出来fileName的目录
    //目录分割符放入到配置文件中。将来跨平台时可根据平台性质转换
    val loc = fileName.lastIndexOf(evm.configuration.Environment.pathSeparator)
    var dir = ""
    println(fileName)
    if (loc >= 0)
      dir = fileName.substring(0, loc + 1)
    app.entities = app.entityDefinitions.map(i => EntitySerializer.deserialize(app, dir + i))
    app.services = app.serviceDefinitions.map(i => ServiceSerializer.deserialize(app, dir + i))

    AppUtil.genCode(app)

    var errors = AppUtil.validate(app) ::: AppUtil.adjust(app)
    if (errors.length > 0) {
      val err = new SchemaException("应用验证错误！" + errors.mkString("\r\n"))
      error("应用程序验证错误！"+errors.mkString("\r\n"),err)
      throw err
    }

    return app
  }

}