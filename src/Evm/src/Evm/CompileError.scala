package evm

/**
 * 编译错误
 */
object CompileError extends Enumeration {
  type CompileError = Value

  val ER0001 = Value("ER0001: 源文件不存在")

  val ER0002 = Value("ER0002: xsd验证错误")

  val ER0003 = Value("ER0003: 存在重复的实体定义[%s]")

  val ER0004 = Value("ER0004: 实体定义错误[%s]：%s 声明返回列表，但语句类型不是select或存储过程")

  val ER0005 = Value("ER0005: 实体定义错误[%s]：%s 声明分页，但未声明返回列表")

  val ER0006 = Value("ER0006: 实体定义错误[%s]：%s是select语句，但没有定义返回值类型(resultClass)")

  val ER0007 = Value("ER0007: 实体定义错误[%s]：%s 非select语句，只能返回int类型")

  val ER0008 = Value("ER0008: 实体定义错误[%s]：属性%s被声明为外键引用，但数据类型不是int或long")

  val ER0009 = Value("ER0009: 实体定义错误[%s]：属性%s被声明为外键引用，但其所引用的主实体或主实体属性并不存在")

  val ER0010 = Value("ER0010: 实体定义错误[%s]：属性%s被声明为外部语句引用(select)，但其所引用的语句并不存在")

  val ER0011 = Value("ER0011: 实体定义错误[%s]：索引%s未包含字段/属性名称")

  val ER0012 = Value("ER0012: 实体定义错误[%s]：属性%s的下拉列表(DropDownListData)引用了不存在的服务方法")

  val ER0013 = Value("ER0013: 实体定义错误[%s]：属性%s的下拉列表(DropDownListData)数据格式不正确。正确的格式是:value1:text1[;value2:text2]")

  val ER0014 = Value("ER0014: 实体定义错误[%s]：属性%s的下拉列表定义(DropDownListData)格式错误")

  val ER0015 = Value("ER0015: 存在重复的服务定义[%s]")

  val ER0016 = Value("ER0016: 服务定义错误[%s]：方法%s定义的嵌入语句引用了不存在的实体语句$(%s).%s")

  val ER0017 = Value("ER0017: 服务定义错误[%s]：方法%s定义的嵌入语句引用了不存在的方法$(%s).%s")

  val ER0018 = Value("ER0018: 服务定义错误[%s]：方法%s定义的嵌入语句引用了不存在的实体或服务$(%s)")

  val ER0019 = Value("ER0019: 服务定义错误[%s]：方法%s的引用了不存在的实体语句%s")

  val ER0020 = Value("ER0020: 服务定义错误[%s]：方法%s引用了了不存在的实体%s")

  val ER0021 = Value("ER0021: 服务定义错误[%s]：方法%s不能同时存在refStatement和embedCode")

  val ER0022 = Value("ER0022: 服务定义错误[%s]：方法%s支持分页或排序，但未声明返回值类型")

  val ER0023 = Value("ER0023: 服务定义错误[%s]：方法%s声明支持(Support)事务，但所引用方法$(%s).%s声明不支持(NotSupport)事务")

  val ER0024 = Value("ER0024: 服务定义错误[%s]：方法%s及所引用方法$(%s).%s均声明需要新的(RequiredNew)事务")

  val ER0025 = Value("ER0025: 服务定义错误[%s]: 方法%s不支持(NotSupport)事务，但所引用方法$(%s).$s均声明需要(Required)事务")

  val ER0026 = Value("ER0026: 服务定义错误[%s]：方法%s的嵌入时语句(embedCode)中存在多个需要新事物的(RequiredNew)语句")

  val ER0027 = Value("ER0027: 安装方法定义错误：引用了不存在的方法%s")

  val ER0028 = Value("ER0028: 安装方法定义错误：签名不符合安装方法要求。期望的方法签名为:void (Version ver,Version last)")

  val ER0029 = Value("ER0029: 卸载方法定义错误：引用了不存在的方法%s")

  val ER0030 = Value("ER0030: 卸载方法定义错误：签名不符合安装方法要求。期望的方法签名为:void ()")
  
  val ER0031 = Value("ER0031: 实体定义错误[%s]：%s 正则验证只能用于字符串类型")
  
  val ER0032 = Value("ER0032: 实体定义错误[%s]：%s 范围(Range)验证只能用于数字或时间类型")

  val ER0033 = Value("ER0033: 实体定义错误[%s]：%s 长度(Length)验证只能用于字符串类型")
  
  val ER0034 = Value("ER0034: 实体定义错误[%s]：%s 范围(Range)验证要求必须同时提供minValue和maxValue")
  
  val ER0035 = Value("ER0035: 服务定义错误[%s]：%s 声明需要新事务(RequiredNew)的方法，Evm要求不能具有返回值")
  
  val WR0001 = Value("WR0001: 事物级别已被修改：方法$(%s).%s的事物级别由%s已自动提升为所引用方法的事物最高级别%s")
}