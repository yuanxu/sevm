/**
 *
 */
package evm
import evm.utils.Event

/**
 * 具体项编译器基类
 *
 */
abstract class EvmObjectCompilerBase {

  /**
   * 是否是实体编译器
   */
  val isEntityCompiler: Boolean

  /**
   * 是否是服务编译器
   */
  val isServiceCompiler: Boolean

  /*
   * 正在编译的应用
   */
  protected var app: schema.App=null

  /**
   * 临时目录
   */
  protected var tempDir: String=""

  /**
   * 输出目录
   */
  protected var outputDir: String=""

  /**
   * 生成的目标文件
   */
  val targetFiles = List[String]()

  /**
   * 主编译器
   */
  var mainCopmiler: CompilerBase=null

  /**
   * 设置编译器
   */
  def setup(app: schema.App, outputDir: String, tempDir: String) {
    this.app = app;
    this.outputDir = outputDir;
    this.tempDir = tempDir;
  }

  /**
   * 执行编译
   */
  protected def doCompile(t: AnyRef): String

  /**
   * 开始编译
   */
  protected def beginCompile(t: AnyRef) {}

  /**
   * 结束编译
   */
  protected def endCompile(t: AnyRef, code: String) {}

  /**
   * 编译
   */
  def compile(t: AnyRef) {
    beginCompile(t);
    val code = doCompile(t);
    endCompile(t, code);
    onCompiledEvmObject(t, code);
  }

  // 事件处理
  private var compiledAll = List[Function1[AnyRef, Unit]]()
  def addAllCompiledHandler(f: Function1[AnyRef, Unit]) { compiledAll ::= f }

  private[evm] def onAllCompile {
    compiledAll.foreach(_(this))
  }

  private var compiled = new Event[EvmObjectCompilerEventArgs]()

  /**
   * 添加单个对象编译完成事件处理器
   */
  def addCompiledHandler(handler: Function2[AnyRef, EvmObjectCompilerEventArgs, Unit]) { compiled += handler }

  private def onCompiledEvmObject(source: AnyRef, target: String) {
    val co = new EvmObjectCompilerEventArgs() {
      this.source = source
      this.target = target
    }
    //触发方法
    compiled(this, co)
  }
}

