package evm
import evm.utils.EventArgs

/**
 * 编译对象
 */
class EvmObjectCompilerEventArgs extends EventArgs{

  /**
   * 编译目标
   */
  var source: AnyRef = null

  /**
   * 编译后的目标码
   */
  var target = ""

  /**
   * 文件名
   */
  var fileName = ""

  /**
   * 写入文件
   */
  def writeToFile(fileName: String) {
    //调用模板类
   utils.TplUtil.writeToFile(fileName,target)
  }
}