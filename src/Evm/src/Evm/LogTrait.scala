package evm
import org.slf4j.LoggerFactory

/**
 * 日志
 */
trait LogTrait {
  private val log = LoggerFactory.getLogger(getClass())

  def debug(msg: String) = log.debug(msg)
  def debug(msg: String, err: Throwable) = log.debug(msg, err)

  def info(msg: String) = log.info(msg)
  def info(msg: String, err: Throwable) = log.info(msg, err)

  def error(msg: String) = log.error(msg)
  def error(msg: String, err: Throwable) = log.error(msg, err)

  def trace(msg: String) = log.trace(msg)
  def trace(msg: String, err: Throwable) = log.trace(msg, err)

  def warn(msg: String) = log.warn(msg)
  def warn(msg: String, err: Throwable) = log.warn(msg, err)

  def isInfoEnabled = log.isInfoEnabled()
  def isErrorEnabled = log.isErrorEnabled()
  def isDebugEnabled = log.isDebugEnabled()

  def isTraceEnabled = log.isTraceEnabled()
  def isWarnEnabled = log.isWarnEnabled()
}

object Log extends LogTrait {

}