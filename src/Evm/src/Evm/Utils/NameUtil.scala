/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package evm.utils

/**
 * Jvm相关工具函数
 */
import scala.util.matching.Regex

object NameUtil {

  private val jpRegex = new Regex("^[a-z]")
  private val jmRegex = new Regex("^[A-Z]")
  
  /**
   * 获取Jvm规范的属性名称
   */
  def getJvmPropertyName(name:String)= getClrName(name)
  
  /**
   * 获取Jvm规范的方法名称
   */
  def getJvmMethodName(name:String)={
    if(jmRegex.findFirstIn(name)!=None)
      name.substring(0,1).toLowerCase + name.substring(1)
    else
      name
  }
  
  def jvmIdName="Id"
  
  /**
   * 获取Clr风格的名字
   */
  def getClrName(name:String)={
     if(jpRegex.findFirstIn(name)!=None)
      name.substring(0,1).toUpperCase + name.substring(1)
    else
      name
  }
  
  def clrIDName="ID"
}
