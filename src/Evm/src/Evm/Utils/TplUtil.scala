package evm.utils
import org.fusesource.scalate.TemplateEngine
import scala.io.Source
import java.io.FileOutputStream
import java.io.OutputStreamWriter

/**
 * 模板类
 */
object TplUtil {

  val engine = new TemplateEngine()

  /**
   * 解析
   */
  def parse(tplName: String, attributes: Map[String, AnyRef] = Map()): String = {
    //val fullName = getClass().getResource(tplName)
    engine.layout(tplName, attributes)
  }

  /**
   * 解析并存入文件中
   */
  def parseToFile(tplName: String, fileName: String, attributes: Map[String, AnyRef] = Map()) {
    writeToFile(fileName, parse(tplName, attributes))
  }

  /**
   * 写入文件
   */
  def writeToFile(fileName: String, content: String) {
    val fos = new FileOutputStream(fileName)
    val osw = new OutputStreamWriter(fos, "UTF-8")
    osw.write(content)
    osw.flush()

    osw.close()
    fos.close()
  }
}