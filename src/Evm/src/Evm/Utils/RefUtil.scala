package evm.utils
import scala.util.matching.Regex
import scala.collection.mutable.HashMap

/**
 * $() 引用工具类
 */
object RefUtil {
  /// <summary>
  /// 用于匹配对象引用的正则
  /// </summary>
  private val REF_MATCH_EXP = """\$\(\w+\).\w+"""

  /// <summary>
  /// 用于分开对象和方法调用的正则
  /// </summary>
  private val REF_SPLIT_EXP = """\$\(|\)."""

  /**
   * 匹配对象用的正则对象
   */
  private val regex = new Regex(REF_MATCH_EXP)

  /*
   * 匹配分割对象的正则对象
   */
  private val splitRegex = new Regex(REF_SPLIT_EXP)

  /**
   * 判断是否含有引用
   */
  def hasRef(input: String) = regex.findFirstIn(input) != None

  /**
   * 分析引用对象和方法
   * @return: Key:对象;Value 方法
   */
  def getRefParts(input: String) = {
    val map = new HashMap[String, List[String]]()
    val matches = regex.findAllIn(input)
    matches.foreach(r => {
      val rt = splitRegex.split(r) //分割$(a)b 为: "",a,b
      val key = rt(1)
      val value = rt(2)
      if (map.contains(key)) {
        val values = map.get(key)
        if (!values.exists(_ == value))
          map(key) = value :: values.get
      } else {
        map += Pair(key, List(value))
      }
    })

    map //return
  }

  /**
   * 获取简单的引用定义，用于只引用了一个对象的一个成员/属性
   */
  def getSimpleRefPart(input: String): Pair[String, String] = {
    val refDef = getRefParts(input)
    if (refDef.isEmpty)
      null
    else {
      val key = refDef.keySet.first
      val value = refDef(key).first
      Pair(key, value)
    }
  }

  /**
   * 替换掉$(xxx)为yyy
   */
  def replace(input: String, source: String, target: String) = {
    val reg = new Regex("""\$\(""" + source + """\)""")
    reg.replaceAllIn(input, target)
  }

  /**
   * 替换$(xxx)为xxx
   */
  def replace(input: String) = {
    if (!hasRef(input))
      input
    val result = splitRegex.split(regex.findPrefixOf(input).get)
    format("%s.%s", result(1), result(2))
  }
}