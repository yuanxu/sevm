package evm.utils

import evm.schema.Entity

/**
 * User: YuanXu
 * Date: 11-11-24
 * Time: 上午9:15
 */
/**
 * 数据库驱动工具
 */
object DbDriverUtil {

  /**
   * 获取selectKey语句
   */
  def  getSelectKey(entity:Entity)=
    {
      val id=  if(entity.app.options.useClrStyle) NameUtil.clrIDName else NameUtil.jvmIdName
      entity.app.options.dbProvider match
      {
        case "sqlServer2.0" | "sqlServer2005"  =>
          <selectKey property={entity.name + id} type="post" resultClass="int" >
            select @@identity
          </selectKey>
       
        case "SQLite3" | "MySql" =>
          <selectKey property={entity.name + id} type="post" resultClass="int" >
            select last_insert_id()
          </selectKey>
        case _ => ""
      }
    }

  /**
   * 获取驱动属性
   */
  def getDriverProperties(dbProvider:String)={
    dbProvider match{
       case "sqlServer2.0" | "sqlServer2005"  =>
           """####################################
          # Database Connectivity Properties
          ####################################
          driver=com.microsoft.sqlserver.jdbc.SQLServerDriver
          url=jdbc:sqlserver://server[:port];DATABASE=[db]
          username=
          password=
          """
       case "javaDb" =>
          """####################################
          # Database Connectivity Properties
          ####################################
          driver=org.apache.derby.jdbc.ClientDriver
          url=jdbc:derby:databaseName;URLAttributes
          #or
          #url=jdbc:derby://host:port/databaseName;URLAttributes
          username=
          password=
          """
       case "MySql" =>
         """####################################
          # Database Connectivity Properties
          ####################################
          driver=com.mysql.jdbc.Driver
          url=jdbc:mysql://server[:port]/[db]
          username=
          password=
          """
       case "db2" =>"COM.ibm.db2.jdbc.app.DB2Driver"
        case _ => ""
    }
  }
}