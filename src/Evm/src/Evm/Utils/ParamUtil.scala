package evm.utils
import scala.collection.mutable.ListBuffer

import scala.util.matching.Regex

/**
 * 参数分解工具
 */
object ParamUtil {

  /**
   * 获取变量和变量类型。
   * @return: typeName，varName
   */
  def getVars(item: String): Pair[String, String] = {
    val v = item.substring(item.indexOf(" ") + 1) //去掉类型，只取变量名
    val typeName = item.substring(0, item.indexOf(" ")).trim() //类型名

    var loc = v.indexOf("=")
    if (loc >= 0)
      Pair(typeName, v.substring(0, loc)) //如果有默认值，则去掉默认值
    else
      Pair(typeName, v.trim())

  }

  /**
   * 获取变量名
   * @return: List[Pair[typeName,varName]]
   */
  def getVarName(item: String) = getVars(item)._1

  /**
   * 拆分参数声明
   */
  def getParameters(parameters: String): List[Pair[String, String]] = {
    if (parameters.isEmpty())
      return List()
    val buffer = ListBuffer[Pair[String, String]]()
    for (item <- splitParameters(parameters)) {
      buffer += getVars(item)
    }
    buffer.toList
  }

  /**
   * 拆分参数
   */
  def splitParameters(parameters: String): List[String] = {

    //使用正则表达式拆分
    //sample: string userName , Role role,IList<int> organIDList ,  IList<KeyValuePair<int,bool>> accountIDList,IList<KeyValuePair<int,int>> 
    val regex = new Regex("""([\w|>]\s+[\w])""")

    //内容：
    //strin
    //serName , Rol
    //ole,IList<int
    //rganIDList ,  IList<KeyValuePair<int,bool>
    //ccountIDList,IList<KeyValuePair<int,int>>

    val parts = regex.split(parameters)

    //分隔符：
    //g u
    //e r
    //> o
    //> a
    val splitParts = (regex.findAllIn(parameters).toList) ::: List("   ")

    assert(parts.length == splitParts.length)

    val tmpBuffer = ListBuffer[String]()
    var stuff = ""
    for (i <- 0 to parts.length - 1) {
      tmpBuffer += stuff + parts(i) + splitParts(i).first
      stuff = splitParts(i).last.toString()
    }

    //之后tmpList存放有如下格式数据：
    //string   
    //userName , Role 
    //role,IList<int> 
    //organIDList ,  IList<KeyValuePair<int,bool>> 
    //accountIDList,IList<KeyValuePair<int,int>> 
    //最后一个变量名存放于parts[parts.Length-1]中,此变量的第一位存于parts[parts.Lenth-2].last()

    val buffer = ListBuffer[String]()
    var typeName = tmpBuffer(0)

    for (i <- 1 to tmpBuffer.length - 2) {
      //拆分：上一个参数的形参名和后一个参数的类型
      val loc = tmpBuffer(i).indexOf(',');
      buffer += format("%s %s", typeName.trim(), tmpBuffer(i).substring(0, loc).trim())
      typeName = tmpBuffer(i).substring(loc + 1); //下一个参数的类型
    }
    //最后一个参数
    var name = tmpBuffer(tmpBuffer.length - 1)
    buffer += format("%s %s", typeName, name)

    buffer.toList
  }
}