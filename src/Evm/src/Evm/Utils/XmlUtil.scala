package evm.utils
import scala.xml.Elem

/**
 * 用于简化xml操作的特性
 */
object XmlUtil {
	def apply(xml : Elem) : XmlUtil={
	  new XmlUtil(xml)
	}
}
  class XmlUtil(val xml: Elem){

  /**
   * 获取节点的值
   */
  def text(elName: String, default: String = ""): String = {
    val nodes = xml \\ elName
    if (nodes.length > 0)
      nodes.first.text
    else
      default
  }
    
  /**
   * 获取节点的属性值
   */
  def attr(elName : String ,attrName : String ,default : String = "") : String ={
      val nodes = xml \\ elName
    if(nodes.length == 0)
      default
      else
        nodes.first.attribute(attrName).getOrElse(default).toString()
  }
}