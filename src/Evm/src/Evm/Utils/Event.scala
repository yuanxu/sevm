package evm.utils
import scala.collection.mutable.ListBuffer

class EventArgs {

}
object EventArgs {
  def apply() = new EventArgs()
}
/**
 * 事件
 */
class Event[T <: EventArgs] extends Function2[AnyRef, T, Unit] {
  type Handler = Function2[AnyRef, T, Unit]
  protected val handlers = ListBuffer[Handler]()

  /**
   * 添加事件处理器
   */
  def addHandler(handler: Handler) {
    handlers += handler
  }

  /**
   * 添加事件处理器
   */
  def +=(handler: Handler) { addHandler(handler) }

  /**
   * 移除事件处理程序
   */
  def removeHandler(handler: Handler) {
    handlers -= handler
  }

  /**
   * 移除事件处理程序
   */
  def -=(handler: Handler) { removeHandler(handler) }

  /**
   * 分发事件
   */
  def apply(sender: AnyRef, e: T) {
    handlers.foreach(_(sender, e))
  }
}

/**
 * 表示没有数据的事件
 */
class EventHandler extends Event[EventArgs] {
  
  def apply() {
    val e = new EventArgs()
    handlers.foreach(_(this, e))
  }
}
