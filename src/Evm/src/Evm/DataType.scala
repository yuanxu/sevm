/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package evm

/**
 * 数据类型
 */
class DataType(val Type:String)  {
  lazy private val lowercaseType = Type.toLowerCase
  
  private val intTypes = List("byte","sbyte","short","ushort","int","uint","long","ulong","bigint")
  
  /**
   * 是否是字符串
   */
  def isString = lowercaseType=="string" 
  
  /**
   * 是否是整数
   */
  def isInteger = intTypes.contains(lowercaseType)
  
  /**
   * 是否是日期
   */
  def isDateTime = lowercaseType == "datetime"
  
  /**
   * 是否浮点数
   */
  def isFloat = lowercaseType =="float"
  
  /**
   * 是否是双精度浮点
   */
  def isDouble  = lowercaseType =="double"
  
  /**
   * 是否是日期或日期时间
   */
  def isDate = lowercaseType == "date" || lowercaseType=="datetime"
  
  /**
   * 是否是Decimal
   */
  def isDecimal = lowercaseType=="decimal" || lowercaseType=="bigdecimal"
  
  /**
   * 是否是数字
   */
  def isNumber = isInteger || isFloat || isDouble || isDecimal
  
  /**
   * 是否是枚举
   */
  var isEnumeration = false
  
  /**
   * 是否是Guid
   */
  var isGuid = lowercaseType == "guid"
  
  /**
   * 是否是对象
   */
  def isObject = !isString && !isNumber && !isDate && !isDecimal && !isEnumeration && !isGuid
  
  /**
   * 获取映射到的JVM类型
   */
  def jvmDataType =DataType.getJvmDataType(lowercaseType)
  
  /**
   * 获取映射到的CLR类型
   */
  def clrDataType = DataType.getClrDataType(lowercaseType)
  
  /**
   * 已重写。返回类型的名称
   */
  override def toString = Type
}

/**
 * Evm数据类型伴生对象
 */
object DataType{
  
  /**
   * 支持的数据类型
   */
  private val SupportedDataTypes = List("byte","short","int","long","decimal","float","double","string","datetime","guid","bool")
  
  /**
   * Jvm 数据映射
   */
  private val JvmDataMap = Map("byte"->"byte","short"->"short","int"->"int","long"->"long","decimal"->"java.math.BigDecimal",
                               "float"->"float","double"->"double","string"->"String","datetime"->"java.sql.Timestamp","guid"->"java.util.UUID"
                               ,"bool" -> "boolean")
  
  /**
   * Clr 数据映射
   */
  private val ClrDataMap = Map("byte"->"byte","short"->"short","int"->"int","long"->"long","decimal"->"decimal",
                               "float"->"float","double"->"double","string"->"string","datetime"->"DateTime","guid"->"Guid"
                               ,"bool" -> "bool")
  
  def apply(value:String)={
    new DataType(value)
  }
  
  /**
   * 隐式转换为字符串
   */
  implicit def dataType2String(Type:DataType) = Type.Type
  
  
  /**
   * 获取映射到的JVM类型
   */
  def getJvmDataType(typeName:String) ={
    if( JvmDataMap.contains(typeName) )
      JvmDataMap(typeName)
    else  typeName
  }
  
  /**
   * 获取映射到的CLR类型
   */
  def getClrDataType(typeName:String) ={
    if (ClrDataMap.contains(typeName))
      ClrDataMap(typeName)
    else
      typeName
  }
  
  /**
   * 是否是支持的数据类型
   */
  def isSuppotedDataType(dataType:String) = SupportedDataTypes.contains(dataType)
}