package evm
import evm.utils.EventHandler
import java.io.File

/**
 * 编译器基类
 */
abstract class CompilerBase {
  /**
   * 日志类
   */
  protected val log = evm.Log

  /**
   * 编译器名称
   */
  val name: String

  /**
   * 编译器友好名称
   */
  val friendlyName: String

  /**
   * 编译器信息
   */
  val information = ""

  /**
   * 配置信息
   */
  var configuration = Map[String, String]()

  var app: schema.App = null

  var outputDir = ""
  var tempDir = ""

  /**
   *
   */
  var evmObjectCompilers = List[EvmObjectCompilerBase]()

  /**
   * 设置编译器参数
   */
  def setup(app: schema.App, outputDir: String, tempDir: String, configuration: Map[String, String] = Map[String, String]()) {
    this.app = app
    this.outputDir = outputDir
    this.tempDir = tempDir
    this.configuration = configuration
    evmObjectCompilers.foreach(_.setup(app, outputDir, tempDir))

  }

  /**
   * 添加入主编译器的对象编译列表中
   */
  def addEvmObjectCompiler(oc: EvmObjectCompilerBase) {
    oc.mainCopmiler = this
    evmObjectCompilers ::= oc

  }

  /**
   * 开始编译事件
   */
  val beginCompile = new EventHandler()

  /**
   * 结束编译事件
   */
  val endCompile = new EventHandler()

  private def onEndCompile() {
    endCompile()
  }

  private def onBeginCompile() {
    beginCompile()
  }

  /**
   * 执行编译过程
   */
  def compile() {
    onBeginCompile //开始编译事件

    //检查文件是否存在
    val file = new File(outputDir)
    if (!file.exists())
      file.mkdirs()

    for (compiler <- evmObjectCompilers) {
      if (compiler.isEntityCompiler) {
        for (entity <- app.entities) {
          compiler.compile(entity)
        }
        compiler.onAllCompile //告知对象编译器已完成所有编译工作
      } else if (compiler.isServiceCompiler) {
        for (svc <- app.services) {
          compiler.compile(svc)
        }
        compiler.onAllCompile //告知对象编译器已完成所有编译工作

      }
    }

    onEndCompile() //所有编译执行完毕
  }
}