package evm.configuration

/**
 * Evm 运行时环境设置
 */
object Environment {

  /**
   * 应用程序(定义)存放目录
   */
  var appFolder = "."

  /**
   * 应用程序bin/.dll目录地址
   */
  var binDir = "."

  /**
   * OutputDir
   */
  var outputDir = "."

  /**
   * 编译器
   */
  var rtlCompilers = Map[String, String]()

  /**
   * Schema同步器
   */
  var schemaSyncers = Map[String, String]()

  /**
   * 路径分隔符
   */
  lazy val pathSeparator ="/"// System.getProperty("file.separator")
}