package evm.schema

/**
 * 服务契约
 */
object ServiceContract extends Enumeration {
  type ServiceContract = Value
  /**
   * 公共服务，提供接口定义
   */
  val Public = Value("Public")

  /**
   *  私有服务，一般用于服务内部的工具类等
   */
  val Private = Value("Private")

  /**
   * 私有对象，实现Sington模式的私有类
   */
  val PrivateObject = Value("PrivateObject")

  def apply(value: String) = {
    value match {
      case "Public"        => Public
      case "Private"       => Private
      case "PrivateObject" => PrivateObject
      case _               => Public
    }
  }
}