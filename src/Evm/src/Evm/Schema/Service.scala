package evm.schema

/**
 * 服务定义
 */
class Service {

  /**
   * 服务名
   */
  var name = ""

  /**
   * 服务的方法定义
   */
  var methods = List[Method]()

  /**
   * 直接引用(派生)的实体名
   */
  var derivedEntityName = ""

  /**
   *  直接引用（派生）的实体类
   */
  var derivedEntity: Option[Entity] = None

  /**
   * 是否根据DerivedEntity生成对应的语句
   */
  var generateMethodsAccordingToDerivedEntity = true

  /**
   * 引用的实体
   */
  var referencedEntites = List[Entity]()

  /**
   * 引用的服务
   */
  var referencedServices = List[Service]()

  /**
   * 文件名
   */
  var fileName = ""

  /**
   * 所属的应用
   */
  var app: App = null

  /**
   * 导入的命名空间
   */
  var imports = List[String]()

  /**
   * 获取最终的接口名
   */
  var interfaceName = if (this.derivedEntityName.isEmpty()) name else this.derivedEntityName

  /**
   * 服务描述
   */
  var summary = ""

  /**
   * 服务契约
   */
  var serviceContract = evm.schema.ServiceContract.Public
  
  
}