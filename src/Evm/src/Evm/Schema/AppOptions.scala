package evm.schema
import scala.reflect.BeanProperty

/**
 *
 * 应用程序配置
 */
class AppOptions() {
  /**
   * 数据库驱动名称
   */
  @BeanProperty
  var dbProvider = ""

  /**
   * 数据源配置.可以使用@引用配置文件中的配置
   */
  @BeanProperty
  var dataSource = ""

  /**
   * 是否同步数据库的Schema。
   */
  @BeanProperty
  var syncSchemaToDB = true

  /**
   * 是否生成默认的方法
   */
  @BeanProperty
  var generateDefaultMethods = true

  /**
   * 是否生成默认的ID和主键
   */
  @BeanProperty
  var generateIdAndPK = true

  /**
   * 是否提升事务级别
   */
  @BeanProperty
  var upgradeTransactionModel = true

  /**
   * 执行安装的方法。
   * 引用service中的方法，语法为:$(service).method
   * 签名为:void install(Version ver,Version lastVer)
   */
  @BeanProperty
  var installMethod = ""

  /**
   * 反安装方法
   * 引用service中的方法，语法为:$(service).method
   * 签名为:void uninstall()
   */
  @BeanProperty
  var uninstallMethod = ""

  /**
   * 是否使用独立的数据库
   */
  @BeanProperty
  var useStandaloneDatabase = true

  /**
   * 是否根据主外键引用生成默认的导航属性
   */
  @BeanProperty
  var generateNavigator = false

  /**
   * 是否成成整型主键
   */
  var generateIntegerId=true
  
  /**
   * 整型主键的数据类型
   */
  var integerIdDataType = "int"
  
  /**
   * 是否使用Clr风格生成方法
   */
  var useClrStyle=false

  /**
   * 目标语言
   */
  var targetLanguage =TargetLanguage.Scala
}