package evm.schema.serializer
import javax.xml.validation.SchemaFactory
import javax.xml.transform.stream.StreamSource
import scala.xml.Node

/**
 *  序列化/反序列化器
 */
trait SerializerTrait {

  /**
   *  验证xsd
   */
  def validate(fileName: String): Boolean

  /**
   * 执行验证
   */
  protected def doValidate(xsdFile: String, fileName: String): Boolean =
  {
    // 1. Lookup a factory for the W3C XML Schema language
    val factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema")

    // 2. Compile the schema.
    // Here the schema is loaded from a java.io.File, but you could use
    // a java.net.URL or a javax.xml.transform.Source instead.
    val xsdUri = getClass().getResource("/evm/schema/resource/"+xsdFile)
    
    val schema = factory.newSchema(xsdUri)

    // 3. Get a validator from the schema.
    val validator = schema.newValidator();

    // 4. Parse the document you want to check.
    val source = new StreamSource(fileName);

    // 5. Check the document
    try {
      validator.validate(source);
      true
    } catch {
      case e @ _ => error(e.getMessage)
        false
    }

  }
  
  /**
   * 获取指定节点的属性值
   */
  protected def attr(node:Node,key: String,default: String = "")={
    val n = node.attribute(key)
    if(n == None)
      default
    else
      n.get.text
  }
}