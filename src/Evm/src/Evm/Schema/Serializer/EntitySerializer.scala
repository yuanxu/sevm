package evm.schema.serializer
import evm.schema._
import scala.io._
import evm.LogTrait
import evm.utils.XmlUtil
import scala.collection.mutable.ListBuffer


/**
 * 实体反序列化器
 */
object EntitySerializer extends SerializerTrait with LogTrait {

  /**
   * 验证xsd
   */
  override def validate(fileName: String) = {
    super.doValidate("EntityDef.xsd", fileName)
  }

  /**
   * 反序列化
   */
  def deserialize(app: App, fileName: String): Entity = {
    //检查文件是否存在
    info(format("开始解析%s...", fileName))

    //验证文件是否完整
    if (!validate(fileName)) {
      error(format("%s验证失败！", fileName));
      throw new SchemaException()
    }

    val doc = XmlUtil(xml.XML.loadFile(fileName))
    val entity = new Entity()
    entity.app = app
    entity.fileName = fileName

    entity.name = doc.attr("entity", "name")
    entity.dbName = doc.attr("entity", "dbName", entity.name)
    entity.isEnum = doc.attr("entity", "isEnum", "false").toBoolean
    entity.text = doc.attr("entity", "text")

    entity.properties = deserializeProperty(entity, doc)
    entity.indexes = deserializeIndex(entity, doc)
    entity.cacheModels = deserializeCacheModel(entity, doc)
    entity.statements = deserializeStatement(entity, doc)

    return entity
  }

  /**
   * 解析属性定义
   */
  private def deserializeProperty(ett: Entity, doc: XmlUtil): List[Property] = {
    (doc.xml \\ "property").map(p => new Property() {
        this.entity = ett
        name = attr(p, "name")
        column = attr(p, "column",name) //数据库名称，如果为空则为Name值
        text = attr(p, "text")
        required = attr(p, "required", "false").toBoolean
        default = attr(p, "default")
        regExp = attr(p, "regExp")
        error = attr(p, "error")
        foreignKey = attr(p, "foreignKey")
        minLength = attr(p, "minLength", "0")
        maxLength = attr(p, "maxLength", "0")
        lazyLoad = attr(p, "lazyLoad", "false").toBoolean
        select = attr(p, "select", "")
        dataType = evm.DataType(attr(p, "type"))
        dbType = attr(p, "dbType")
        percision = attr(p, "percision", "10").toInt
        scale = attr(p, "scale", "2").toInt
        columnIndex = attr(p, "columnIndex", "-1").toInt
        formatString = attr(p, "formatString")
        noMap = attr(p, "noMap", "false").toBoolean
        minValue = attr(p, "minValue")
        maxValue = attr(p, "maxValue")
        controller = attr(p, "controller", "textbox")
        dropDownListData = attr(p, "dropDownListData", "")
        fileExtNames = attr(p, "fileExtNames")
        isAutoGenerated = attr(p, "isAutoGenerated", "false").toBoolean

      }).toList
  }

  /**
   * 解析索引定义
   */
  private def deserializeIndex(ett: Entity, doc: XmlUtil): List[Index] = {
    (doc.xml \\ "index").map(p => new Index() {
        this.entity = ett
        name = attr(p, "name")
        columns = attr(p, "columns")
        isUnique = attr(p, "isUnique", "false").toBoolean

      }).toList
  }

  /**
   * 解析缓存模型
   */
  private def deserializeCacheModel(ett: Entity, doc: XmlUtil): List[CacheModel] = {
    (doc.xml \\ "cacheModel").map(p => {
        new CacheModel() {
          this.entity = ett
          id = attr(p, "id")
          readOnly = attr(p, "ReadOnly", "false").toBoolean
          serialize = attr(p, "serialize", "false").toBoolean
          implementation = CacheModelImplementation(attr(p, "implementation", "FIFO"))
          flushOnExecutes = (p \\ "flushOnExecute").map(fp =>
            new FlushOnExecute() { statement = attr(fp, "statement") }).toList

          properties = (p \\ "property").map(fp => {
              new CacheModelProperty() {
                name = attr(fp, "name")
                value = attr(fp, "value")
              }
            }).toList

          val fiTmp =  (doc.xml \\ "flushInterval").map(fp => {
              new FlushInterval() {
                hours = attr(fp, "hours", "0").toInt
                minutes = attr(fp, "minutes", "0").toInt
                seconds = attr(fp, "seconds", "0").toInt
                milliSeconds = attr(fp, "milliseconds", "0").toInt
              }
            })
          if(!fiTmp.isEmpty)
            flushInterval = fiTmp.head //仅有一个FlushInterval节
        }
      }).toList
  }

  /**
   * 解析语句
   */
  private def deserializeStatement(ett: Entity, doc: XmlUtil): List[Statement] = {
    val buffer = ListBuffer[Statement]()
    for (
      p <- (doc.xml \\ "statements") \\ "_" if (p.label == "select" || p.label == "insert" || p.label == "update" ||
                                                p.label == "delete" || p.label == "procedure" || p.label == "sql")
                                                  ) {
          buffer += new Statement() {
            tagName = p.label
            this.entity = ett
            id = attr(p, "id")
            isAutoGenerated = false
            parameters = attr(p, "parameters")
            resultClass = attr(p, "resultClass")
            listClass = attr(p, "listClass")
            summary = attr(p, "summary")
            supportPaging = attr(p, "supportPaging", "false").toBoolean
            supportSort = attr(p, "supportSort", "false").toBoolean
            innerXml = p.child.mkString.replaceAll("xmlns=\""+SchemaConstants.ENTITY_SCHEMA_URI+"\"", "")
            cacheModel = attr(p,"cacheModel","")
            extendStatement = attr(p, "extends")
          }
        }
      buffer.toList
      }

      }

