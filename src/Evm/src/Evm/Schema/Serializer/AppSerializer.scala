package evm.schema.serializer
import javax.xml._
import javax.xml.validation._
import evm.utils.XmlUtil
import evm.LogTrait
import evm.schema._

object AppSerializer extends SerializerTrait with LogTrait {

  /**
   * 验证
   */
  override def validate(fileName: String) = {
    super.doValidate("AppDef.xsd", fileName)
  }

  def deserialize(fileName: String): App = {

    info(format("开始解析%s...", fileName))

    //验证文件是否完整
    if (!validate(fileName)) {
      error(format("%s验证失败！", fileName));
      throw new SchemaException()
    }

    val app = new evm.schema.App()
    val doc = XmlUtil(xml.XML.loadFile(fileName))

    app.name = doc.text("name")
    app.description = doc.text("description")
    app.author = doc.text("author")
    app.namespace = doc.text("namespace")
    app.version = Version(doc.text("version"))

    app.options.dbProvider = doc.text("dbProvider")
    app.options.syncSchemaToDB = doc.text("syncSchemaToDB", "true").toBoolean
    app.options.generateIdAndPK = doc.text("generateIdAndPK", "true").toBoolean
    app.options.generateNavigator = doc.text("genearteNavigator", "false").toBoolean
    app.options.installMethod = doc.text("installMethod")
    app.options.uninstallMethod = doc.text("uninstallMethod")
    app.options.upgradeTransactionModel = doc.text("UpgradeTransactionModel", "true").toBoolean
    app.options.dataSource = doc.text("dataSource")
    app.options.useStandaloneDatabase = doc.text("useStandaloneDatabase", "false").toBoolean
    app.options.generateIntegerId = doc.text("generateIntegerId", "true").toBoolean
    app.options.integerIdDataType = doc.text("integerIdDataType", "int")
    app.options.useClrStyle=doc.text("useClrStyle","false").toBoolean
    app.options.targetLanguage = TargetLanguage(doc.text("targetLanguage","scala"))


    app.assemblies = (doc.xml \\ "assembly").map(_.text).toList
    app.imports = (doc.xml \\ "import").map(_.text).toList
    app.entityDefinitions = (doc.xml \\ "entity").map(_.text).toList
    app.serviceDefinitions = (doc.xml \\ "service").map(_.text).toList

    info(format("%s解析成功!应用名称:%s,版本:%s", fileName, app.name, app.version))

    return app
  }
}