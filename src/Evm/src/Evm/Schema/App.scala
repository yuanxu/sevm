package evm.schema

import scala.reflect.BeanProperty
import javax.xml.bind.annotation._

/**
 * Evm 应用程序定义
 */

class App() {

  /**
   * 应用程序名称
   */
  @BeanProperty
  var name = ""

  /**
   * 程序版本号
   */
  @BeanProperty
  var version = new Version()

  /**
   * 作者
   */
  @BeanProperty
  var author = ""

  /**
   * 详细说明
   */
  @BeanProperty
  var description = ""

  /**
   * 应用的默认命名空间
   */
  @BeanProperty
  var namespace = ""

  /**
   * 引入的程序集(dll/jar)
   */
  @BeanProperty
  var assemblies: List[String] = List[String]()

  /**
   * 默认导入的命名空间
   */
  var imports: List[String] = List[String]()

  /**
   * 服务定义文件集
   */
  @BeanProperty
  var serviceDefinitions: List[String] = List[String]()

  /**
   * 实体定义文件集
   */
  @BeanProperty
  var entityDefinitions: List[String] = List[String]()

  /**
   * 应用程序配置
   */
  @BeanProperty
  var options = new AppOptions()

  /**
   * 所有实体定义
   */
  var entities = List[Entity]()

  /**
   * 所有服务定义
   */
  var services = List[Service]()

  private var m_allStatements = List[Statement]()

  private var m_allMethods = List[Method]()

  private var m_allProperties = List[Property]()
  /**
   * 所有的语句
   */
  def allStatements = {
    if (m_allStatements.length == 0)
      for (e <- entities)
        m_allStatements :::= e.statements

    m_allStatements
  }

  /**
   * 所有的方法
   */
  def allMethods = {
    if (m_allMethods.length == 0)
      for (s <- services)
        m_allMethods :::= s.methods

    m_allMethods
  }

  /**
   * 所有的属性
   */
  def allProperties = {
    if (m_allProperties.length == 0)
      for (e <- entities)
        m_allProperties :::= e.properties

    m_allProperties
  }
}