package evm.schema

/**
 * 事物级别
 */
object TransactionModel extends Enumeration {
  type TransactionModel = Value
  /**
   *  不支持
   */
  val NotSupport = Value("NotSupport")

  /**
   * 支持事务，可在事务及非事务环境运行
   */
  val Support = Value("Support")

  /**
   * 需要事务
   */
  val Required = Value("Required")

  /**
   * 需要新事务
   */
  val RequiredNew = Value("RequiredNew")

  def apply(value: String) = value match {
    case "RequiredNew" => RequiredNew
    case "Required"    => Required
    case "NotSupport"  => NotSupport
    case "Support" | _ => NotSupport
  }
}