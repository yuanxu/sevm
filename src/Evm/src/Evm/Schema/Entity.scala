package evm.schema

/**
 * 实体定义
 */
class Entity {

  /**
   * 实体名称
   */
  var name = ""

  /**
   * 实体表名，默认同实体名称(Name)
   */
  var dbName = ""

  /**
   * 属性定义
   */
  var properties = List[Property]()

  /**
   * 缓存模型
   */
  var cacheModels = List[CacheModel]()

  /**
   * 索引定义
   */
  var indexes =  List[Index]()

  /**
   * 所属应用
   */
  var app: App = null

  /**
   * 文件名
   */
  var fileName = ""

  /**
   * 实体友好名字
   */
  var text = ""

  /**
   * 是否是枚举类型
   */
  var isEnum = false
  
  var statements = List[Statement]()
}