package evm.schema

/**
 * Created by IntelliJ IDEA.
 * User: YuanXu
 * Date: 11-11-18
 * Time: 下午2:20
 * To change this template use File | Settings | File Templates.
 */

object TargetLanguage extends Enumeration {
  type TargetLanguage = Value

  val Java = Value("java")

  val Scala = Value("scala")

  val CSharp=Value("c#")

  def apply(value:String)={
    value match {
      case "java" => Java
      case "scala" =>Scala
      case "cSharp" =>CSharp
    }
  }
}