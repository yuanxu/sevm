package evm.schema
import evm.EvmException
import evm.CompileError

class SchemaException(Message: String, InnerException: Exception) extends EvmException(CompileError.ER0002.id, Message, InnerException) {
  def this(message: String) = this(message, null)
  def this() = this("", null)
}