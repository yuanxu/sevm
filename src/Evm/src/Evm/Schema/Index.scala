package evm.schema

/**
 * 索引定义
 */
class Index {

  /**
   * 索引名称
   */
  var name = ""

  /**
   * 索引字段
   */
  var columns = ""

  /**
   * 是否是唯一索引
   */
  var isUnique = false

  /**
   * 所属实体
   */
  var entity: Entity = null
}