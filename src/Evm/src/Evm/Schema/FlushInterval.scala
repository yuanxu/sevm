package evm.schema

/**
 * 缓存刷新间隔
 */
class FlushInterval {
  /**
   * 时
   */
  var hours = 0

  /**
   * 分
   */
  var minutes = 1

  /**
   * 秒
   */
  var seconds = 0

  /**
   * 毫秒
   */
  var milliSeconds = 0
}