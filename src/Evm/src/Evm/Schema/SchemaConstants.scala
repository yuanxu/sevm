package evm.schema

/**
 * schema constants
 */
object SchemaConstants {
  
  /**
   * App schema uri 
   */
  val APP_SCHEMA_URI = "http://evm.org/app"
	
  val ENTITY_SCHEMA_URI="http://evm.org/entity"
  
  val SERVICE_SCHEMA_URI="http://evm.org/service"
}