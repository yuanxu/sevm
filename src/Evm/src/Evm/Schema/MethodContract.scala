package evm.schema

/**
 * 方法约束
 */
object MethodContract extends Enumeration {
  type MethodContract = Value
  val Normal = Value("Normal")
  val Json = Value("Json")
  val Private = Value("Private")

  def apply(value: String) = value match {
    case "Private"    => Private
    case "Json"       => Json
    case "Normal" | _ => Normal
  }
}