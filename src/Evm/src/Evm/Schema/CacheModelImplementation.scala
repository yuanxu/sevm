package evm.schema

/**
 * 缓存实现类型
 */
object CacheModelImplementation extends Enumeration {
  type CacheModelImplementation = Value
  val LRU = Value("LRU")
  val MEMORY = Value("MEMORY")
  val FIFO = Value("FIFO")

  def apply(value: String) = {
    value match {
      case "LRU"      => LRU
      case "MEMORY"   => MEMORY
      case "FIFO" | _ => FIFO
    }
  }
}