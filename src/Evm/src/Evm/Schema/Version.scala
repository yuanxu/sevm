package evm.schema

class Version(var major: Int, var minor: Int, var build: Int, var revision: Int) extends Ordered[Version] {
  def this() = this(0, 0, 0, 0)
  def this(major: Int) = this(major, 0, 0, 0)
  def this(major: Int, minor: Int) = this(major, minor, 0, 0)
  def this(major: Int, minor: Int, build: Int) = this(major, minor, build, 0)

  def compare(that: Version): Int = {
    if (this.major > that.major) //主版本大于
      1
    else if (this.major < that.major) //主版本小于
      -1
    else if (this.minor > that.minor) //主版本相等，比较次版本大于
      1
    else if (this.minor < that.minor) //次版本小于
      -1
    else if (this.build > that.build) //次版本相等，比较build版
      1
    else if (this.build < that.build) //build版小于
      -1
    else if (this.revision > that.revision) //build相等，最后看修订版
      1
    else if (this.revision == that.revision)
      0
    else
      -1
  }
  //String.format("%1$.%2$.%3$.%4$",major,minor,build,revision)
  override def toString(): String = major.toString() + "." + minor.toString() + "." + build.toString() + "." + revision.toString()

}
object Version {
  def apply(ver: String) = {
    assert(ver != null && ver.length() > 0)
    val parts = ver.split('.')
    val major = if (parts.length >= 1) parts(0).toInt else 0
    val minor = if (parts.length >= 2) parts(1).toInt else 0
    val build = if (parts.length >= 3) parts(2).toInt else 0
    val revision = if (parts.length >= 4) parts(3).toInt else 0
    new Version(major, minor, build, revision)
  }

  /**
   * 最小版本
   */
  val MinVersion = new Version()
}