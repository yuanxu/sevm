package evm.schema

/**
 * 缓存模型
 */
class CacheModel {

  /**
   * 唯一标识
   */
  var id = ""

  /**
   * 是否序列化
   */
  var serialize = false

  /**
   * 是否只读
   */
  var readOnly = false

  /**
   * 缓存实现方式
   */
  var implementation = CacheModelImplementation.FIFO

  /**
   * 刷新缓存的语句
   */
  var flushOnExecutes = List[FlushOnExecute]()

  /**
   * 刷新间隔
   */
  var flushInterval = new FlushInterval()

  /**
   * 属性
   */
  var properties = List[CacheModelProperty]()

  /**
   * 所属实体
   */
  var entity: Entity = null
}