/**
 * User: YuanXu
 * Date: 11-12-26
 * Time: 上午8:17
 */
package org.sevm.mapper.test

import org.sevm.mapper.{Sql, Mapper}


class SqlTest{
  def simpleTest={
    //val sql = queryForList[String]("select * from table")
   val mapper = new Mapper
    val i = 3
    val r = mapper.queryForObject[String](Sql("select * from table").dynamic("where", i == 2,"3=2"))

  }
}

class userDao{
  val mapper = new Mapper
  def GetList(orginId:Int) = {
     mapper.queryForList[Int]("select * from user where userId=$(UserId):int",orginId)
  }
}