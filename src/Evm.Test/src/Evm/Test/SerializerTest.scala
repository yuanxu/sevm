package evm.Test
import org.junit._
import evm.schema.Serializer._
import org.junit.runner.RunWith
import org.junit.runners.Suite
import evm.schema.App
import org.junit.Assert
import evm.schema.App
import scala.util.matching.Regex


@RunWith(classOf[Suite])
@Suite.SuiteClasses(Array(classOf[SerializerTest], classOf[JsonTest]))
class TestSuit


class SerializerTest {
  @Test
  def appParse() {
    println("*************appParse*****************")
    val app = evm.AppBuilder.Parse("""D:\Codes\evm.Account\src\evm.Account.Core\app.xml""")
    println(app.Options.toString())
    println(app.ServiceDefinitions.mkString(","))

    println(app.EntityDefinitions.mkString(","))

  }

  @Test
  def ParamUtilsTest {
    val p = "string userName , Role role,IList<int> organIDList ,  IList<KeyValuePair<int,bool>> accountIDList,IList<KeyValuePair<int,int>> holdRoles"

    //string   
    //userName , Role 
    //role,IList<int> 
    //organIDList ,  IList<KeyValuePair<int,bool>> 
    //accountIDList,IList<KeyValuePair<int,int>> 

    val r = evm.utils.ParamUtil.GetParameters(p).mkString("\r\n")
    println(r)
  }
 @Test
  def CjcCompile{
    val cjc = new evm.Compilers.JavaCompiler.CoreJavaCompiler()
    val app = evm.AppBuilder.Parse("""D:\Codes\evm.Account\src\evm.Account.Core\app.xml""")
    cjc.Setup(app,"""D:\Codes\sevm\src\out""","""D:\Codes\sevm\src\out\temp""")
    cjc.Compile()
  }
  
}