package evm.test
import evm.utils.EventArgs
import org.junit._
import org.fusesource.scalate.TemplateEngine

class EventTest {
  @Test
  def TestEvent {
    val mc = new MainClass()
    mc.AddHandler()
    mc.OnHello()

  }

  @Test
  def TplTest {
    val engine = new TemplateEngine()
    val path = getClass().getResource("../../Sample/Tpl.ssp").getPath()
    println(getClass().getResource("../../Sample/Tpl.ssp").getPath())
    val r =  engine.layout(path, Map( "Hello" -> List("Hello", "Scalate!") ))
    println(r)
    
    println(engine.layout("D:/Codes/sevm/src/evm.Test/src/Sample/Tpl.ssp", Map( "Hello" -> List("Hello", "Scalate","again!") )))
  }
}

class MainClass {
  private val helloEvent = new evm.utils.Event[HelloEventArgs]()

  def AddHandler() {
    helloEvent += Handler
  }

  def OnHello() {
    helloEvent(this, new HelloEventArgs() { Message = "Hello from MainClass" })
  }

  private def Handler(sender: AnyRef, e: HelloEventArgs) {
    println("From Handler:" + e.Message)
  }
}

class HelloEventArgs extends EventArgs {
  var Message = ""
}