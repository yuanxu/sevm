package evm.rtl

import com.ibatis.sqlmap.engine.impl.ExtendedSqlMapClient
import com.ibatis.sqlmap.engine.transaction.{TransactionManager, TransactionConfig}
import java.io.{FileNotFoundException, File}
import evm.AppBuilder
import io.Source
import collection.mutable.{HashMap, ListBuffer}
import evm.configuration.Environment
import com.google.inject.{Module, Guice, Injector}

/**
 * User: YuanXu
 * Date: 11-11-24
 * Time: 上午8:30
 */

private[rtl] trait EngineAppTrait {

  //已经加载的app
  protected val loadedApps = new ListBuffer[EvmApp]()

  //已经加载的模块
  protected val loadedModules =  HashMap[String,Injector]()

  /**
   * 构建应用
   */
  def installApp(fileName : String){
    val file = new File(fileName)
    if(!file.exists())
      throw new FileNotFoundException()

    if(fileName.endsWith(".zip")){
      //TODO:先解压，然后再分析
    }

    val app = AppBuilder.parse(fileName)


  }

  /**
   * 加载应用
   * @param appName 应用程序名
   * @param syncSchema 是否同步Schema
   */
  def load(appName:String,syncSchema:Boolean = true){
    //检查是否加载过
    if(loadedApps.exists(_.name == appName))
      return
    val app = Registry.getByName(appName)
    if(app == None){
      throw new EvmRtException("应用%1$s不存在.".format(appName))
    }
    //load
   val inj = loadModule(app.get)

    //init app instance
    initAppInstance(inj,app.get)
  }

  private def loadModule(app:EvmApp):Injector={
    val module = Class.forName("%s.%sModule".format(app.namespace,app.name))

  val inj = Guice.createInjector(module.newInstance().asInstanceOf[Module])
    loadedModules.put(app.name,inj)

    inj
  }

  private def initAppInstance(inj:Injector,app:EvmApp){
    val ins = inj.getInstance(classOf[AppBase])
    val source = Source.fromFile("%s/%s/SqlMapConfig.xml".format(Environment.appFolder,app.name))
    //ins.mapper = SqlMapClientBuilder.buildSqlMapClient(source.reader())
  
  }

}