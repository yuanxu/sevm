package evm.rtl
import evm.utils.Event
import evm._
import java.io.{FileNotFoundException, File}
import collection.mutable.{HashMap, ListBuffer}
import com.google.inject.{Injector, Module, Guice}
import java.lang.ClassNotFoundException
import com.ibatis.sqlmap.client.SqlMapClientBuilder
import scala.io.Source

/**
 * Evm 引擎
 */
object Engine extends EngineAppTrait {

  //对外发布消息的事件
  val onMessage = new Event[EngineEventArgs]()

  //发布消息
  private def publishMessage(sender: AnyRef, message: String) = onMessage(sender, new EngineEventArgs(message))



  private val cachedInjectors = HashMap[String,Injector]()
  /**
   * 解析
   */
  def resolve[T](t:Class[T]):T={
    val cname = t.getName
    if(cachedInjectors.contains(cname))
    {
      return cachedInjectors.get(cname).get.getInstance(t)
    }
   for(kv <- loadedModules){
     val inj = kv._2
     try{
       cachedInjectors.put(cname,inj)
     return inj.getInstance(t)
     }catch {
       case _ => // nothing to do
     }
   }
    throw new ClassNotFoundException()
  }

  /**
   * 解析
   */
  def resolve(className:String):AnyRef = resolve(Class.forName(className)).asInstanceOf[AnyRef]
  
}