package evm.rtl
import evm.rtl.validate.Validator
import scala.collection.mutable.ListBuffer
import scala.reflect.BeanInfo

/**
 * 实体/模型基类
 */
@BeanInfo
abstract class ModelBase {

  /**
   * 校验错误集合
   */
  private var m_ruleViolations : ListBuffer[RuleViolation]=null
  
  /**
   * 获取验证结果集合
   */
  def ruleVialations = {
    validate
    m_ruleViolations
  }
  
  /**
   * 验证器
   */
  protected val validators = ListBuffer[Validator]()
  /**
   * 是否校验通过
   */
  def isValid =  {
      validate
    m_ruleViolations.isEmpty
  }
  
  private def validate(){
    if(m_ruleViolations!=null) {
      return
    }
    prepareValidator
    m_ruleViolations =  new ListBuffer[RuleViolation]()
    for(v <- validators){
      if(!v.isValid)
        m_ruleViolations += new RuleViolation(v.name,v.errorMessage)
    }
  }

  /**
   * 添加验证器
   */
  protected def addValidator(validator:Validator){
    validators += validator
  }
  
  /**
   * 准备验证器
   */
  protected def prepareValidator
}