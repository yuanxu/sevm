package evm.rtl
import com.alibaba.fastjson.JSON
import scala.reflect.BeanProperty

/**
 * 调用结果
 */
class InvokeResult(@BeanProperty val errorCode: Int, @BeanProperty val errorMessage: String, @BeanProperty val data: Any) {

  /**
   * 成功无数据
   */
  def this() = this(0, "", null)

  /**
   * 包含成功数据
   */
  def this(data: Any) = this(0, "", data)

  /**
   * 错误
   */
  def this(errorCode: Int, errorMessage: String) = this(errorCode, errorMessage, null)

  def this(errorCode: Int) = this(errorCode, "", null)

  @BeanProperty
  val isSucceed = errorCode == 0

  @BeanProperty
  val isError = errorCode != 0

  /**
   * 转换为Json
   */
  def toJson = JSON.toJSONString(this, true)

}

/**
 * 类型解析错误
 */
class InvokeTypeResolveError(typeName: String) extends InvokeResult(-1, format("Resolve Type %s Exception.", typeName)) {
  def this() = this("")
}

/**
 * 未找到方法
 */
class InvokeMethodNotFoundError(method: String) extends InvokeResult(-2, format("Method %s Not Found Exception", method)) {
  def this() = this("")
}

/**
 * 参数错误
 */
class InvokeArgumentException(message:String) extends InvokeResult(-3,message){
  def this() = this("Arguments Null Exception")
}

/**
 * 验证错误
 */
class InvokeAuthenticationError extends InvokeResult(-4,"Authentication Error")

/**
 * 鉴权错误
 */
class InvokeAuthorizationError extends InvokeResult(-5,"Authorization error")

/**
 * 未知错误
 */
class InvokeUnknownException(message:String) extends InvokeResult(-1000,format("Unknown Error!\r\n%s",message)){
  def this()=this("")
}