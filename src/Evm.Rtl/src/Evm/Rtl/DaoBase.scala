package evm.rtl

import java.util.HashMap

/**
 * Dao基类
 */
abstract class DaoBase {
  
  protected def getSortHashtable(ht: HashMap[String, Any], sortColumn: String, sortDirection: String) {
    if (!sortColumn.isEmpty()) {
      {
        ht.put("sortColumn", sortColumn)
        ht.put("sortDirection", sortDirection)
      }
    }
    ht
  }
  
  protected def getSortHashtable(sortColumn: String, sortDirection: String) {
    getSortHashtable(new HashMap(), sortColumn, sortDirection);
  }

  /* <summary>
   获取当前服务的映射
   </summary>
  */
   protected def getMapper() :SqlMapClient

  /**
   * 将object转换为int类型
   */
  protected def getInt(o:AnyRef,nullValue:Int = -1):Int = {
    if(o == null)
      nullValue
    else
      o.toString().toInt
  }
}