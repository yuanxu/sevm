package evm.rtl
import scala.collection.mutable.ListBuffer
import evm.schema.serializer.SerializerTrait
import evm.schema.Version
import evm.schema.AppOptions
import evm.AppBuilder
import scala.xml.Elem
import java.io.File
import evm.utils.TplUtil
import java.io.FileReader

/**
 * 应用注册表/系统配置
 */
object Registry extends SerializerTrait {

  private val apps = ListBuffer[EvmApp]()
  private lazy val dbName = getClass().getResource("Registry.xml").getPath()

  //装载
  load

  private def load {
    val doc = xml.XML.loadFile(dbName)
    (doc \\ "app").foreach(item =>
      apps += new EvmApp() {
        this.name = attr(item, "name")
        author = attr(item, "author")
        description = attr(item, "description")
        namespace = attr(item, "namespace")
        version = evm.schema.Version(attr(item, "version"))
        executedInstall = attr(item, "executedInstall").toBoolean
        syncedSchema = attr(item, "syncedSchema").toBoolean
        lastVersion = evm.schema.Version(attr(item, "lastVersion"))
        options = new AppOptions() {
          dataSource = attr(item, "dataSource")
          dbProvider = attr(item, "dbProvider")
          installMethod = attr(item, "installMethod")
          uninstallMethod = attr(item, "uninstallMethod")
          useStandaloneDatabase = attr(item, "useStandaloneDatabase").toBoolean
        }
      })
  }

  /**
   * 获取所有已经编译/上载的应用
   */
  def getList = apps

  /**
   * 根据名称获取
   */
  def getByName(appName: String) = apps.find(_.name == appName)

  /**
   * 获取最后一个版本
   */
  def getLast(appName: String) = {
    var app = getByName(appName)
    if (app == None)
      None
    else {
      val ap = app.get
      EvmApp(AppBuilder.parse(format("%s\\%s\\%s\\app.xml", evm.configuration.Environment.appFolder, ap.name, ap.version.toString())))
    }
  }

  /**
   * 根据版本获取
   */
  def getByVersion(appName: String, ver: String) = {
    var app = getByName(appName)
    if (app == None)
      None
    else
      EvmApp(AppBuilder.parse(format("%s\\%s\\%s\\app.xml", evm.configuration.Environment.appFolder, app.get.name, ver)))
  }

  /**
   * 命名空间是否存在
   */
  def namespaceExists(nameSpace: String) = apps.exists(_.namespace == nameSpace)

  /**
   * 移除应用
   */
  def remove(appName: String) {
    val app = getByName(appName)
    if (app != None)
      apps -= app.get
  }

  /**
   * 添加应用
   */
  def add(app: evm.schema.App) {
    apps += EvmApp(app)
  }

  /**
   * 保存
   */
  def save {
    val doc = <registry>{
        apps.map(app => <app name={ app.name } version={ app.version.toString() } author={ app.author } namespace={ app.namespace } description={ app.description } dataSource={ app.options.dataSource } dbProvider={ app.options.dbProvider } installMethod={ app.options.installMethod } uninstallMethod={ app.options.uninstallMethod } executedInstall={ app.executedInstall.toString() } syncedSchema={ app.syncedSchema.toString() } useStandaloneDatabase={ app.options.useStandaloneDatabase.toString() } lastVersion={ app.lastVersion.toString() }></app>)
      }</registry>
   
      TplUtil.writeToFile(dbName, doc.toString())
      }

      //为了应付SerializerTrait
      override def validate(fileName: String) = true
      }