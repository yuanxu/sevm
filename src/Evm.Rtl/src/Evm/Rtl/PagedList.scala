package evm.rtl
import scala.reflect.BeanInfo
import java.util.List
/**
 * 分页的列表
 */
@BeanInfo
class PagedList[T <: ModelBase](val pageIndex: Int,
                                val recordCount: Int,
                                val data: List[T],
                                var pageSize: Int = 20) {

  def pageCount = if (recordCount % pageSize == 0) recordCount / pageSize else recordCount / pageSize + 1

  override def toString() = format("共有%d条数据，第%d页，每页%d条数据", recordCount, pageIndex, pageSize)
}