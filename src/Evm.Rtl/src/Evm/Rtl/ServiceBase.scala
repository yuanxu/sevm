package evm.rtl

/**
 * 服务基类
 * 核心编译器应该为每个具体服务生成如下几个函数：
 *  AssertInTransaction 
 *  AssertNotInTransaction
 *  AssertTransactionNotStart
 *  BeginTransaction
 *  Commit
 *  Rollback
 */

abstract class ServiceBase {


  /**
   * 确保运行于事物中
   */
  protected def assertInTransaction()
  {
    //getMapper().
  }

  /**
   * 确保不在事务中
   */
  protected def assertNotInTransaction()
  {
    
  }

  /**
   * 确保事务未启动
   */
  protected def assertTransactionNotStart()
  {

  }

  /**
   * 启动事务
   */
  protected def beginTransaction()
  {
    getMapper().startTransaction
  }

  /**
   * 提交事务
   */
  protected def commit()
  {
    getMapper().commitTransaction
  }

  /**
   * 回滚事物
   */
  protected def rollback()
  {
    getMapper().getCurrentConnection.rollback()
    
  }
  
  /* <summary>
   获取当前服务的映射
   </summary>
   */
  protected def getMapper() :SqlMapClient
}