package evm.rtl.validate
import evm.DataType
/**
 * 验证基类
 */
abstract class Validator {
  /**
   * 是否合法
   */
  var isValid: Boolean =false
  
  /**
   * 错误消息
   */
  var errorMessage: String=""
  
  /**
   * 属性
   */
  val name:String
  
  /**
   * 属性有好名
   */
  val text:String
  
  /**
   * 数据类型
   */
  val dataType:DataType
  
  /**
   * 属性值
   */
  val value:Any
  
  /**
   * 验证
   */
  def validate
}