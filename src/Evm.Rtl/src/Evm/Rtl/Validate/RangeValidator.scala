/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package evm.rtl.validate
import evm.DataType
/**
 * 范围验证
 */
class RangeValidator(override val name:String,override val text:String,override val dataType:DataType,override val value: Any,
                     val minValue:String,val maxValue:String) extends Validator {

  
  override def validate(){
    val err = format("%s 的值必须在%s - %s之间",text,minValue,maxValue)
    val svalue = value.toString
    
    dataType.toString match{
      case "byte" =>
        
        val v = svalue.toByte
        if( v < minValue.toByte || v > maxValue.toByte)
          errorMessage = err
      case "short" =>
        val v = svalue.toShort
        if( v < minValue.toShort || v > maxValue.toShort)
          errorMessage = err
      case "int" => 
        val v = svalue.toInt
        if(v < minValue.toInt || v > maxValue.toInt)
          errorMessage = err
      case "long" =>
        val v = svalue.toLong
        if(v < minValue.toLong || v > maxValue.toLong)
          errorMessage = err
      case "float" =>
        val v = svalue.toFloat
        if( v < minValue.toFloat || v > maxValue.toFloat)
          errorMessage = err
      case "double" =>
        val v = svalue.toDouble
        if( v < minValue.toDouble || v > maxValue.toDouble)
          errorMessage = err
      case "decimal" | "bigdecimal" =>
        
        val v = new java.math.BigDecimal(svalue)
        if( v.compareTo(new java.math.BigDecimal(minValue)) == -1 || v.compareTo(new java.math.BigDecimal(maxValue))==1)
          errorMessage = err
        //TODO:增加DateTime类型支持
        /* case "datetime" => // for clr
         val v = */
      case _ => throw new evm.rtl.NotSupportedDataType("RangeValidator不支持的数据类型"+DataType)
    }
  }
}
