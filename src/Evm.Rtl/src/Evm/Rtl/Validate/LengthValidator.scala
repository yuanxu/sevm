package evm.rtl.validate
import evm.rtl.NotSupportedDataType
import evm.DataType
/**
 * 长度验证
 */
case class LengthValidator(override val name:String,override val text:String,override val dataType:DataType,override val value: Any
                           , minLength: Int, maxLength: Int) extends Validator {
  override def validate{
    dataType.toString match{
      case "string" =>
        val v = value.asInstanceOf[String]
        isValid = v.length() >= minLength && v.length() <= maxLength
        errorMessage = if (isValid) "" else format("%s 的长度必须在%d-%d之间", text, minLength, maxLength)
      case _ => throw new NotSupportedDataType("LengthValidator仅支持string或String类型")
    }
    
  }
  
}