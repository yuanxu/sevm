package evm.rtl.validate
import evm.DataType

/**
 * 必须性验证
 */
case class RequiredValidator(override val name:String,override val text:String,override val dataType:DataType,override val value: Any) extends Validator {
 	
  override def validate{
    dataType.toString match{
      case "string" => 
        isValid = value.asInstanceOf[String].isEmpty
        if(!isValid)
          errorMessage = format("%s 不能为空",text)
      case _ => 
        isValid = value == null
        errorMessage = format("%s 不能为空",text)
    }
  }
}