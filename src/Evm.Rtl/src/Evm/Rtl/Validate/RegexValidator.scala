/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package evm.rtl.validate
import evm.DataType
import scala.util.matching.Regex

class RegexValidator(override val name:String,override val text:String,override val dataType:DataType,override val value: Any,val regex:String,val errMsg:String)  extends Validator{
  override def validate{
    assert(dataType.isString)
   
    val reg = new Regex(regex)
    if(reg.findFirstIn(value.asInstanceOf[String])==None)        {
      isValid = false
      errorMessage = format("%s %s",text,errMsg)
    }
    else{
      isValid = true
    }

  }
}
