package evm.rtl

import scala.reflect.BeanProperty

/**
 * 运行时错误
 */
class EvmRtException(@BeanProperty val errorCode: Int, val message: String, @BeanProperty val innerException: Exception) extends Exception {

  def this(message: String) = this(-9999, message, null)
  def this(errorCode: Int, message: String) = this(errorCode, message, null)
  def this() = this(-9999, "", null)
}