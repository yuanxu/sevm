package evm.rtl
import evm.schema.App
import evm.schema.Version

private[rtl] class EvmApp extends evm.schema.App {
  /**
   * 是否执行了安装
   */
  var executedInstall = false

  /**
   * 最后版本
   */
  var lastVersion = new Version()

  /**
   * 是否同步了Schema
   */
  var syncedSchema = false

}

object EvmApp {
  def apply(app: evm.schema.App) = {
    new EvmApp() {
      assemblies = app.assemblies
      author = app.author
      description = app.description
      entities = app.entities
      entityDefinitions = app.entityDefinitions
      imports = app.imports
      name = app.name
      namespace = app.namespace
      options = app.options
      serviceDefinitions = app.serviceDefinitions
      services = app.services
      version = app.version

      lastVersion = new Version()
      executedInstall = false
      syncedSchema = false
    }
  }
}