package evm.rtl


/**
 * 应用积累
 */
abstract class AppBase {

  /**
   * 应用名称
   */
  val name: String

  /**
   * 应用描述
   */
  val description: String

  /**
   * 应用版本
   */
  val version: evm.schema.Version

  /**
   * 作者
   */
  val author: String


  /**
   * Sql映射。作为一个契约，被核心编译器写入到最终的实现中
   */
 var mapper:SqlMapClient=null


}