package evm.rtl
import evm.utils.EventArgs
import scala.reflect.BeanProperty

/**
 * 引擎事件参数
 */
class EngineEventArgs(@BeanProperty val message: String) extends EventArgs 