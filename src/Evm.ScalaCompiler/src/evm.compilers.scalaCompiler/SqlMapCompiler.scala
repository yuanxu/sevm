package evm.compilers.scalaCompiler


import evm.EvmObjectCompilerBase
import evm.Log
import evm.utils.TplUtil
import java.io.File

private[scalaCompiler] class SqlMapCompiler (override val isEntityCompiler: Boolean = true,
                   override val isServiceCompiler: Boolean = false) extends EvmObjectCompilerBase{

   /**
   * 执行编译
   */
  override def doCompile(t: AnyRef): String = {
    try {
      t match {
        case entity: evm.schema.Entity => 
           TplUtil.parse(getClass().getResource("templates/SqlMap.ssp").getPath(), Map("entity" -> t))
        case _ => ""
      }
    } catch {
      case e @ _ =>
        Log.error(e.getMessage())
        throw e
    }
  }

  /**
   * 结束编译
   */
  override def endCompile(t: AnyRef, code: String) {
    super.endCompile(t, code)
    val pathName = format("%s/apps/%s/src/%s/dao/maps", tempDir,app.name, app.namespace.split('.').mkString("/"))
    val file = new File(pathName)
    
    if (!file.exists())
      file.mkdirs()
    TplUtil.writeToFile(format("%s/%s.xml", pathName, (t.asInstanceOf[evm.schema.Entity]).name), code)
  }
}

