package evm.compilers.scalaCompiler

import evm.CompilerBase
import evm.utils.TplUtil
import java.io.File
import evm.configuration.Environment
import evm.utils.DbDriverUtil

/**
 * 核心Scala编译器
 */
class CoreScalaCompiler(override val name: String = "scalaCompiler",
                       override val friendlyName: String = "Core Scala Compiler v1.0",
                       override val information: String = "Evm核心Scala编译器")
extends CompilerBase {

  private val appCompiler = new AppCompiler()
  this.addEvmObjectCompiler(appCompiler)

  private val moduleCompiler = new AppModuleCompiler()
  this.addEvmObjectCompiler(moduleCompiler)

  private val modelCompiler = new ModelCompiler()
  this.addEvmObjectCompiler(modelCompiler)

  private val idaoCompiler = new IDaoCompiler()
  this.addEvmObjectCompiler(idaoCompiler)

  private val isvcCompiler = new IServiceCompiler()
  this.addEvmObjectCompiler(isvcCompiler)

  private val daoImplCompiler = new DaoImplCompiler()
  this.addEvmObjectCompiler(daoImplCompiler)

  private val sqlMapCompiler = new SqlMapCompiler()
  this.addEvmObjectCompiler(sqlMapCompiler)

  private val svcImplCompiler = new ServiceImplCompiler()
  this.addEvmObjectCompiler(svcImplCompiler)
  
  override def compile {
    
    for (entity <- app.entities) {
      if (!entity.isEnum){
        modelCompiler.compile(entity)
        idaoCompiler.compile(entity)
        daoImplCompiler.compile(entity)
        sqlMapCompiler.compile(entity)
      }
    }
    
    for(svc <- app.services){
      isvcCompiler.compile(svc)
      svcImplCompiler.compile(svc)
    }

    appCompiler.compile(app)
    moduleCompiler.compile(app)

    genTypes
    
    getSqlMap
    
    getPom
  }
  
  /**
   * 生成别名
   */
  private def genTypes(){
    val pathName = format("%s/apps/%s/src/%s/dao/maps", tempDir,app.name, app.namespace.split('.').mkString("/"))
    val file = new File(pathName)
    
    if (!file.exists())
      file.mkdirs()
    val code= TplUtil.parse(getClass().getResource("templates/TypeAlias.ssp").getPath(), Map("app" -> app))
    TplUtil.writeToFile(format("%s/%sTypes.xml", pathName, app.name), code)
  }
  
  private def getSqlMap{
     val pathName = format("%s/apps/%s/src/%s/dao/maps", tempDir,app.name, app.namespace.split('.').mkString("/"))
    val file = new File(pathName)
    
    if (!file.exists())
      file.mkdirs()
    var code= TplUtil.parse(getClass().getResource("templates/SqlMapConfig.ssp").getPath(), Map("app" -> app))
    //重置配置属性位置
    val resFile = "%s/%s.properties".format(Environment.outputDir,app.name)
    code = code.replace("@resFile","file://"+resFile)
    TplUtil.writeToFile(format("%s/SqlMapConfig.xml", pathName), code)

    //写入属性文件
    TplUtil.writeToFile(resFile,DbDriverUtil.getDriverProperties(app.options.dbProvider))
  }
  
  private def getPom{
     val pathName = format("%s/apps/%s/", tempDir,app.name)
    val file = new File(pathName)
    
    if (!file.exists())
      file.mkdirs()
    val code= TplUtil.parse(getClass().getResource("templates/pom.ssp").getPath(), Map("app" -> app))
    TplUtil.writeToFile(format("%s/pom.xml", pathName), code)
  }
}