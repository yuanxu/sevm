/**
 * User: YuanXu
 * Date: 11-11-30
 * Time: 上午10:14
 */

package evm.compilers.scalaCompiler
import evm.utils.TplUtil
import evm.{Log, EvmObjectCompilerBase}
import java.io.File

/**
 * App 编译器
 */
private[scalaCompiler] class AppCompiler (override val isEntityCompiler: Boolean = true,
                      override val isServiceCompiler: Boolean = false) extends EvmObjectCompilerBase{
   /**
   * 执行编译
   */
  override def doCompile(t: AnyRef): String = {
    try {
      t match {
        case app: evm.schema.App =>
          TplUtil.parse(getClass().getResource("templates/App.ssp").getPath(), Map("app" -> t))
        case _ => ""
      }
    } catch {
      case e @ _ =>
        Log.error(e.getMessage())
        throw e
    }
  }

  /**
   * 结束编译
   */
  override def endCompile(t: AnyRef, code: String) {
    super.endCompile(t, code)
    val pathName = format("%s/apps/%s/src/%s", tempDir,app.name, app.namespace.split('.').mkString("/"))
    val file = new File(pathName)

    if (!file.exists())
      file.mkdirs()
    TplUtil.writeToFile(format("%s/%sApp.java", pathName, (t.asInstanceOf[evm.schema.App]).name), code)
  }
}